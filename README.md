# LiveValidator - Theme Bootstrap3

[![build status](https://gitlab.com/LiveValidator/Theme-Bootstrap3/badges/master/build.svg)](https://gitlab.com/LiveValidator/Theme-Bootstrap3/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Theme-Bootstrap3/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Theme-Bootstrap3/commits/master)

Bootstrap3 themes for LiveValidator. These Bootstrap3 themes should integrate seamlessly with any project that already uses Bootstrap3.

Three options exist to show input errors:
- `help-block` from the form component
- Tooltips via its component
- Popovers via its respective JS component

Find the project [home page and docs](https://livevalidator.gitlab.io/).
