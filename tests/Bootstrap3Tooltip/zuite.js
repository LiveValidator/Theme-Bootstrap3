/**
 * The test suite for the bootstrap3Tooltip theme "class" (Bootstrap3Tooltip.js)
 */

/* globals bootstrap3Tooltip */
describe( 'Bootstrap3Tooltip theme', function() {
    describe( 'check instantiation', function() {
        bootstrap3Tooltip.instantiationSpec();
    } );
    describe( 'check `addErrors` when', function() {
        bootstrap3Tooltip.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        bootstrap3Tooltip.clearErrorsSpec();
    } );
} );
