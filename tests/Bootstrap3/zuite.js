/**
 * The test suite for the bootstrap3 theme "class" (Bootstrap3.js)
 */

/* globals bootstrap3 */
describe( 'Bootstrap3 theme', function() {
    describe( 'check instantiation', function() {
        bootstrap3.instantiationSpec();
    } );
    describe( 'check `markRequired` when', function() {
        bootstrap3.markRequiredSpec();
    } );
    describe( 'check `unmarkRequired` when', function() {
        bootstrap3.unmarkRequiredSpec();
    } );
    describe( 'check `setMissing` when', function() {
        bootstrap3.setMissingSpec();
    } );
    describe( 'check `unsetMissing` when', function() {
        bootstrap3.unsetMissingSpec();
    } );
    describe( 'check `addErrors` when', function() {
        bootstrap3.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        bootstrap3.clearErrorsSpec();
    } );
} );
