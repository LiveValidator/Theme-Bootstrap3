/**
 * The test suite for the bootstrap3Popover theme "class" (Bootstrap3Popover.js)
 */

/* globals bootstrap3Popover */
describe( 'Bootstrap3Popover theme', function() {
    describe( 'check instantiation', function() {
        bootstrap3Popover.instantiationSpec();
    } );
    describe( 'check `addErrors` when', function() {
        bootstrap3Popover.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        bootstrap3Popover.clearErrorsSpec();
    } );
} );
