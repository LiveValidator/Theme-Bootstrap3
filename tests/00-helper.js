var helper = helper || {};

helper.bareInput = function() {
    setFixtures( '<input />' );
    return document.getElementsByTagName( 'input' )[ 0 ];
};

helper.getRow = function() {
    setFixtures( '<div class="form-group"><label for="input">Label</label><input class="form-control" /></div>' );

    return document.getElementsByClassName( 'form-group' )[ 0 ];
};
